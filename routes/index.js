var express = require('express');
var router = express.Router();

const { Pool } = require('pg');
const db = new Pool ({
  database: 'pgsql',
  user: 'postgres',
  password: 'postgres11',
  host: 'localhost',
  port: 5432
});
/* POST (INSERT)*/
router.post('/', async function(req, res, next) {
  const toDb = await db.connect();
  await toDb.query (`INSERT INTO "user" (id, name) VALUES (1001, 'Dmitry');
                     INSERT INTO "user" (id, name) VALUES (1002, 'Vasia');
                     INSERT INTO "user" (id, name) VALUES (1003, 'Petia');
                     INSERT INTO "user" (id, name) VALUES (1004, 'Uriy');
                     INSERT INTO "user" (id, name) VALUES (1005, 'Natalia');`);
  const result = await toDb.query('SELECT * FROM "user"');
  res.render('index', { title: result.rows.map(row => JSON.stringify(row))});
});

/* GET (SELECT)*/
router.get('/', async function(req, res, next) {
  const toDb = await db.connect();
  const result = await toDb.query(`SELECT * FROM "user";`);
  res.render('index', { title: result.rows.map(row => JSON.stringify(row)) });
});

/* PUT (UPDATE)*/
router.put('/', async function(req, res, next) {
  const toDb = await db.connect();
  await toDb.query (`UPDATE "user" SET name='Daria' WHERE name='Dmitry';
                     UPDATE "user" SET name='Oksana' WHERE name='Uriy';`);
  const result = await toDb.query('SELECT * FROM "user";');
  res.render('index', { title: result.rows.map(row => JSON.stringify(row))});
});

/* DELETE (DELETE)*/
router.delete('/', async function(req, res, next) {
  const toDb = await db.connect();
  // await toDb.query (`DELETE FROM "user" WHERE name = 'Daria';`);
  await toDb.query (`DELETE FROM "user";`);
  const result = await toDb.query('SELECT * FROM "user";');
  res.render('index', { title: result.rows.map(row => JSON.stringify(row))});
});



module.exports = router;
